// TODO maybe make singleton! or Avoid this class!!!
export class ContextHolder {

    private _gameContext: CanvasRenderingContext2D;
    private _gameField: HTMLCanvasElement;
    private _scoresTextElement: HTMLElement;
    private _restartButton: HTMLButtonElement;

    public constructor() {
        if (!this._gameContext) {
            this.initContext()
        }
    }

    public get gameContext(): CanvasRenderingContext2D {
        return this._gameContext;
    }

    get gameField(): HTMLCanvasElement {
        return this._gameField;
    }

    get scoresTextElement(): HTMLElement {
        return this._scoresTextElement;
    }

    get restartButton(): HTMLButtonElement {
        return this._restartButton;
    }

    public initContext(): CanvasRenderingContext2D {
        this._gameField = <HTMLCanvasElement>document.getElementById("game-field");
        this._scoresTextElement = document.getElementById("scores-value");
        this._restartButton = <HTMLButtonElement>document.getElementById("restart-btn");
        this._gameContext = this._gameField.getContext('2d');
        if (!this._gameContext) {
            console.error("Can't render smth because context is not defined or nulls'")
        }
        return this._gameContext;
    }
}
