import {ContextHolder} from "../context-holder";
import {GameObjects} from "../model/game-objects";
import {FieldRenderer} from "./field-renderer";
import {SnakeRenderer} from "./snake-renderer";
import {Direction} from "../model/direction";
import {AppleRenderer} from "./apple-renderer";
import {Observer} from "rxjs";

export class GameRenderer {

    // TODO отказаться от холдера скорее всего и просто сделать контекст иниттер, в котором инитить и гетать
    // TODO возможно надо сделтаь контекст синглтоном все таки
    //      чтобы каждый раз за ним не лазить в холдер и не таскать в аргументах
    private readonly contextHolder;

    // TODO отказаться от класса гейм обджектс и хранить игровые объекты в конкретных рендерерах,
    //  а из них просто получать и все. Возможно и нет. Поудмать
    private readonly gameObjects;

    // TODO потом переделать чтобы принимал енам и передавать конкретный ивент
    private readonly restartEventObserver: Observer<boolean>;

    private readonly fieldRenderer;
    private readonly snakeRenderer;
    private readonly appleRenderer;

    // счётчик повторов движений, чтобы не двигаться бесконечно и не двигаться при первом рендере
    private isFirstLaunch = true;
    // флаг того, что в данный момент змея поворачивает (нужен чтобы не делать лишних сдвигов в момент поворота)
    private isTurnActive = false;
    // счётчик очков
    private scores = 0;

    constructor(contextHolder: ContextHolder,
                restartEventObserver: Observer<boolean>,
                gameObjects: GameObjects) {
        this.contextHolder = contextHolder;
        this.gameObjects = gameObjects;
        this.restartEventObserver = restartEventObserver;
        this.fieldRenderer = new FieldRenderer(this.contextHolder.gameContext, this.gameObjects.field);
        this.snakeRenderer = new SnakeRenderer(this.contextHolder.gameContext, this.gameObjects.snake);
        this.appleRenderer = new AppleRenderer(this.contextHolder.gameContext, this.gameObjects.apple);
    }

    public render() {
        if (this.isFirstLaunch) {
            this.reRenderAllObjects();
            this.isFirstLaunch = false;
        } else {
            // не нужно запускать движение вперед, если происходит поворот или рендерим змейку впервый раз.
            if (!this.isTurnActive) {
                this.moveSnake(this.gameObjects.snake.headDirection);
            }
        }
    }

    private reRenderAllObjects() {
        this.clearAllRenderedObjects();
        this.fieldRenderer.render();
        this.appleRenderer.render();
        this.snakeRenderer.render();
    }

    public clearAllRenderedObjects() {
        this.contextHolder.gameContext.clearRect(
            0,
            0,
            this.contextHolder.gameField.clientWidth,
            this.contextHolder.gameField.clientHeight
        );
    }

    public turnSnake(turnDirection: Direction) {
        if (!this.canSnakeBeTurned(turnDirection)) {
            return;
        }
        // TODO проверить необходимость этого флага
        this.isTurnActive = true;
        this.moveSnake(turnDirection);
        this.isTurnActive = false;
    }

    private canSnakeBeTurned(turnDirection: Direction): boolean {
        // не нужно поворачивать змейку, если она уже повернута по нужной оси
        return !(this.isVerticalDirection(turnDirection) && this.gameObjects.snake.hasVerticalDirection()
            || this.isHorizontalDirection(turnDirection) && this.gameObjects.snake.hasHorizontalDirection());
    }

    private isVerticalDirection(turnDirection: Direction): boolean {
        return Direction.UP === turnDirection || Direction.DOWN === turnDirection;
    }

    private isHorizontalDirection(turnDirection: Direction): boolean {
        return Direction.RIGHT === turnDirection || Direction.LEFT === turnDirection;
    }

    private moveSnake(turnDirection: Direction) {
        const currentHeadCell = this.gameObjects.snake.getHead();
        const newHeadCell = this.gameObjects.field.getNextCellByDirection(currentHeadCell, turnDirection);

        if (!newHeadCell) {
            console.log("Коллизия с полем (несуществующая ячейка)");
            this.restartEventObserver.next(true);
            return;
        }
        if (this.gameObjects.snake.coordinates.includes(newHeadCell)) {
            console.log("Коллизия с хвостом");
            this.restartEventObserver.next(true);
            return;
        }

        this.gameObjects.snake.updateHeadWithDirection(newHeadCell, turnDirection);
        const hasSnakeCollisionWithApple = this.gameObjects.snake.hasCollision(this.gameObjects.apple.coordinates);
        if (hasSnakeCollisionWithApple) {
            // TODO вынести в метод апдейт скорс или в отдельный класс скорс менеджер
            this.contextHolder.scoresTextElement.innerHTML = String(++this.scores);
            this.appleRenderer.apple = this.gameObjects.generateApple();
        }
        if (!hasSnakeCollisionWithApple && !this.isFirstLaunch) {
            this.gameObjects.snake.removeTail();
        }
        this.reRenderAllObjects();
    }
}
