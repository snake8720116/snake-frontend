import {ContextHolder} from "../context-holder";

export class RectangleRenderer {
    //
    // private readonly _renderer: RectangleRenderer;
    //
    // private constructor() {
    //     if (!this._renderer) {
    //         this._renderer = new RectangleRenderer();
    //     }
    // }
    //
    // get renderer(): RectangleRenderer {
    //     return this._renderer;
    // }

    // TODO заменить статик на DI с синглтонами
    public static renderSquare(sideSize: number) {
       new ContextHolder().gameContext.fillRect(200, 200, sideSize, sideSize);
    }
}
