import {Snake} from "../model/snake";
import {Cell} from "../model/field/cell";
import {Direction} from "../model/direction";

export class SnakeRenderer {

    private readonly gameContext: CanvasRenderingContext2D;
    private readonly snake: Snake;

    constructor(gameContext: CanvasRenderingContext2D, snake: Snake) {
        this.gameContext = gameContext;
        this.snake = snake;
    }

    public render() {
        // TODO для задания цветов придумать класс управления рисованием и цветами
        for (let i = 0; i < this.snake.coordinates.length; i++) {
            const position = this.snake.coordinates[i];
            // TODO повыносить в методы drawHead, drawBody
            if (i === 0) {
                // задаём цвет тела змейки
                this.gameContext.fillStyle = '#0c6da1';
                this.drawRoundedFilledRectInCell(position, this.getHeadRectangleRoundRadius());
            } else {
                // задаём цвет тела змейки
                this.gameContext.fillStyle = '#49a8de';
                this.drawFilledRectInCell(position);
            }
            position.makeBusy();
        }
    }

    private getHeadRectangleRoundRadius(): number[] {
        let radius;
        switch (this.snake.headDirection) {
            case Direction.UP:
                // TODO вынести 8 в константы
                radius = [8, 8, 0, 0];
                break;
            case Direction.DOWN:
                radius = [0, 0, 8, 8];
                break;
            case Direction.RIGHT:
                radius = [0, 8, 8, 0];
                break;
            case Direction.LEFT:
                radius = [8, 0, 0, 8];
                break;
        }
        return radius;
    }

    // TODO этот метод вынести в ректангл рендерер
    private drawFilledRectInCell(cell: Cell) {
        this.gameContext.fillRect(
            cell.colNumber * cell.sideSize,
            cell.rowNumber * cell.sideSize,
            cell.sideSize,
            cell.sideSize
        );
    }

    // TODO этот метод вынести в ректангл рендерер
    private drawRoundedFilledRectInCell(cell: Cell, radius: number[]) {
        this.gameContext.beginPath();
        this.gameContext.roundRect(
            cell.colNumber * cell.sideSize,
            cell.rowNumber * cell.sideSize,
            cell.sideSize,
            cell.sideSize,
            radius
        );
        this.gameContext.fill();
        this.gameContext.closePath();
    }
}
