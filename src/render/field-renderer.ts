import {Field} from "../model/field/field";

export class FieldRenderer {

    private readonly gameContext: CanvasRenderingContext2D;
    private readonly field: Field;

    public constructor(gameContext: CanvasRenderingContext2D, field: Field) {
        this.gameContext = gameContext;
        this.field = field;
    }

    public render() {
        this.gameContext.beginPath();
        this.gameContext.fillStyle = '#343333';
        this.gameContext.strokeStyle = '#000000';
        this.gameContext.rect(
            0,
            0,
            this.field.realWidthByXCoordinate,
            this.field.realHeightByYCoordinate
        );
        this.gameContext.fill();
        this.gameContext.stroke();
        this.gameContext.closePath();
    }

    /** Deprecated. Renders the field with cells. */
    public renderWithCells() {
        // TODO вынести цвета в константы
        this.gameContext.strokeStyle = '#303433';
        for (let rowNum = 0; rowNum < this.field.height; rowNum++) {
            for (let colNum = 0; colNum < this.field.width; colNum++) {
                const cell = this.field.getCell(rowNum, colNum);
                // TODO use RectangleRenderer.renderSquareByCell(Cell cell); or other method of rectangle renderer
                //  instead of next code
                this.gameContext.strokeRect(
                    colNum * cell.sideSize,
                    rowNum * cell.sideSize,
                    cell.sideSize,
                    cell.sideSize
                );
            }
        }
    }
}
