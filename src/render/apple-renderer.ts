import {Apple} from "../model/apple";
import {Cell} from "../model/field/cell";

export class AppleRenderer {

    private readonly gameContext: CanvasRenderingContext2D;
    private _apple: Apple;

    constructor(gameContext: CanvasRenderingContext2D, apple: Apple) {
        this.gameContext = gameContext;
        this._apple = apple;
    }

    set apple(value: Apple) {
        this._apple = value;
    }

    public render() {
        this.gameContext.fillStyle = '#EF1E1E';
        this.drawFilledCircleInCell(this._apple.coordinates);
    }

    // TODO этот метод вынести в окружность рендерер (отдельный класс)
    private drawFilledCircleInCell(cell: Cell) {
        this.gameContext.beginPath();
        this.gameContext.arc(
            // TODO в cell сделать метод получения реальных координат (cell.colNumber * cell.sideSize)
            cell.colNumber * cell.sideSize + cell.sideSize / 2,
            cell.rowNumber * cell.sideSize + cell.sideSize / 2,
            cell.sideSize / 2,
            0,
            Math.PI * 2,
            true
        );
        this.gameContext.fill();
        this.gameContext.closePath();
    }
}
