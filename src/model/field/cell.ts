export class Cell {

    // TODO возможно имеет смысл храинть тут еще x и y левой верхней точки

    private readonly _colNumber: number;
    private readonly _rowNumber: number;
    private readonly _sideSize: number;
    private _isFree: boolean;

    constructor(colNumber: number,
                rowNumber: number,
                sideSize: number) {
        this._colNumber = colNumber;
        this._rowNumber = rowNumber;
        this._sideSize = sideSize;
        this._isFree = true;
    }

    get colNumber(): number {
        return this._colNumber;
    }

    get rowNumber(): number {
        return this._rowNumber;
    }

    get sideSize(): number {
        return this._sideSize;
    }

    get isFree(): boolean {
        return this._isFree;
    }

    public makeBusy() {
        this._isFree = false;
    }

    public makeFree() {
        this._isFree = true;
    }
}
