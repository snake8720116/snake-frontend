import {Cell} from "./cell";
import {Direction} from "../direction";

export class Field {

    // TODO вынести размер ячейки в свойства конфигурации
    private readonly CELL_SIDE_SIZE = 20;

    private readonly _width: number;
    private readonly _realWidthByXCoordinate: number;
    private readonly _height: number;
    private readonly _realHeightByYCoordinate: number;
    private readonly _cells: Cell[][] = [];

    constructor(width: number, height: number) {
        this._width = width;
        this._realWidthByXCoordinate = this.width * this.CELL_SIDE_SIZE;
        this._height = height;
        this._realHeightByYCoordinate = this.height * this.CELL_SIDE_SIZE;
        this.initCells();
    }

    get width(): number {
        return this._width;
    }

    get realWidthByXCoordinate(): number {
        return this._realWidthByXCoordinate;
    }

    get height(): number {
        return this._height;
    }

    get realHeightByYCoordinate(): number {
        return this._realHeightByYCoordinate;
    }

    // TODO распутать код + пояснить что number это номер ячейки начинающийся с 1 и до width/heigth,
    //  а index - с нуля и до width/heigth - 1


    // TODO refactor this method, may be crete class Row! to avoid multiple arrays
    private initCells() {
        for (let rowNum = 0; rowNum < this._height; rowNum++) {
            this._cells.push([]);
            for (let colNum = 0; colNum < this._width; colNum++) {
                this._cells[rowNum].push(new Cell(colNum, rowNum, this.CELL_SIDE_SIZE));
            }
        }
    }

    public getRow(index: number): Cell[] {
        return this._cells[index];
    }

    public getCell(indexRow: number, indexCol: number): Cell {
        return this._cells[indexRow][indexCol];
    }

    public getCenterCell(): Cell {
        const middleRowNumber = Math.round(this._height / 2);
        const middleColNumber = Math.round(this._width / 2);
        return this._cells[middleRowNumber - 1][middleColNumber - 1];
    }

    public getRandomFreedomCell(): Cell {
        const randomRowNumber = Math.floor(Math.random() * this.height);
        const randomColNumber = Math.floor(Math.random() * this.width);
        const randomCell = this._cells[randomRowNumber][randomColNumber];
        return randomCell.isFree ? randomCell : this.getRandomFreedomCell();
    }

    public getNextCellByDirection(sourceCell: Cell, direction: Direction): Cell {
        let nextCell: Cell;
        switch (direction) {
            case Direction.UP:
                nextCell = this.getTopRelatedCell(sourceCell);
                break;
            case Direction.DOWN:
                nextCell = this.getBottomRelatedCell(sourceCell);
                break;
            case Direction.RIGHT:
                nextCell = this.getRightRelatedCell(sourceCell);
                break;
            case Direction.LEFT:
                nextCell = this.getLeftRelatedCell(sourceCell);
                break;
        }
        return nextCell;
    }

    public getTopRelatedCell(sourceCell: Cell): Cell {
        const isNotLastCell = sourceCell.rowNumber !== 0;
        return isNotLastCell ? this._cells[sourceCell.rowNumber - 1][sourceCell.colNumber] : null;
    }

    public getBottomRelatedCell(sourceCell: Cell): Cell {
        const isNotLastCell = sourceCell.rowNumber !== this.height - 1;
        return isNotLastCell ? this._cells[sourceCell.rowNumber + 1][sourceCell.colNumber] : null;
    }

    public getRightRelatedCell(sourceCell: Cell): Cell {
        const isNotLastCell = sourceCell.colNumber !== this.width - 1;
        return isNotLastCell ? this._cells[sourceCell.rowNumber][sourceCell.colNumber + 1] : null;
    }

    public getLeftRelatedCell(sourceCell: Cell): Cell {
        const isNotLastCell = sourceCell.colNumber !== 0;
        return isNotLastCell ? this._cells[sourceCell.rowNumber][sourceCell.colNumber - 1] : null;
    }
}
