import {GameRenderer} from "../render/game-renderer";
import {ContextHolder} from "../context-holder";
import {GameObjects} from "./game-objects";
import {Observer} from "rxjs";
import {Direction} from "./direction";

export class Game {

    // TODO подумать, может вынести в енам 4 константы ниже
    private readonly UP_EVENTS_KEY: string[] = ["Up", "ArrowUp"];
    private readonly DOWN_EVENTS_KEY: string[] = ["Down", "ArrowDown"];
    private readonly RIGHT_EVENTS_KEY: string[] = ["Right", "ArrowRight"];
    private readonly LEFT_EVENTS_KEY: string[] = ["Left", "ArrowLeft"];

    // айдишка сет интервала, сохраняем для последующего удаления/перезапуска сет интервала
    private frameRefresherId: NodeJS.Timer;

    private gameRenderer: GameRenderer;

    public start() {
        const contextHolder = new ContextHolder();
        contextHolder.initContext();
        // TODO в него передавать функцию onScoreUpdate
        const gameObjects = new GameObjects();
        gameObjects.init();

        contextHolder.gameField.width = gameObjects.field.realWidthByXCoordinate;
        contextHolder.gameField.height = gameObjects.field.realHeightByYCoordinate;

        // TODO создать по человечески класс имплементацию
        const restartEventObserver: Observer<boolean> = {
            next: isRestartNeeded => this.restart(gameObjects, restartEventObserver, contextHolder),
            error: err => {
            },
            complete: () => {
            }
        };

        this.gameRenderer = new GameRenderer(contextHolder, restartEventObserver, gameObjects);

        window.addEventListener("keydown", event => this.processKeyboardEvent(event));

        this.frameRefresherId = this.initializeFrameRefresher();

        contextHolder.restartButton.addEventListener("click", () => {
            this.restart(gameObjects, restartEventObserver, contextHolder);
        })
    }

    private processKeyboardEvent(event: KeyboardEvent) {
        const newSnakeDirection = this.getDirectionByArrowKey(event);
        if (newSnakeDirection === null) {
            return;
        }
        this.gameRenderer.turnSnake(newSnakeDirection);

        // TODO разобраться что этот метод делает
        // Cancel the default action to avoid it being handled twice
        event.preventDefault();
    }

    private getDirectionByArrowKey(event: KeyboardEvent): Direction {
        if (this.UP_EVENTS_KEY.includes(event.key)) {
            return Direction.UP;
        } else if (this.DOWN_EVENTS_KEY.includes(event.key)) {
            return Direction.DOWN;
        } else if (this.RIGHT_EVENTS_KEY.includes(event.key)) {
            return Direction.RIGHT;
        } else if (this.LEFT_EVENTS_KEY.includes(event.key)) {
            return Direction.LEFT;
        } else {
            return null;
        }
    }

    private restart(gameObjects: GameObjects,
                    restartEventObserver: Observer<boolean>,
                    contextHolder: ContextHolder) {
        clearInterval(this.frameRefresherId);
        this.gameRenderer.clearAllRenderedObjects();
        gameObjects.init();
        contextHolder.scoresTextElement.innerHTML = "0";
        this.gameRenderer = new GameRenderer(contextHolder, restartEventObserver, gameObjects);
        this.frameRefresherId = this.initializeFrameRefresher();
    }

    private initializeFrameRefresher(): NodeJS.Timer {
        // TODO увеличить fps до 30-50, когда будет скорость змейки. Пока используем 5.
        const fps = 5;// 1 for debug;
        // метод выполняет функцию рендер fps раз в секунду
        return setInterval(() => {
            this.gameRenderer.render();
            // TODO скорость змейки должна настраиваться отдельно от фпс
        }, 1000 / fps);
    }
}
