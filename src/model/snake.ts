import {Cell} from "./field/cell";
import {Direction} from "./direction";

export class Snake {

    private _coordinates: Cell[];
    private _headDirection: Direction;

    constructor(coordinates: Cell[], headDirection: Direction) {
        this._coordinates = coordinates;
        this._headDirection = headDirection;
    }

    get coordinates(): Cell[] {
        return this._coordinates;
    }

    get headDirection(): Direction {
        return this._headDirection;
    }

    public getHead(): Cell {
        return this.coordinates[0];
    }

    public updateHeadWithDirection(newHeadCell: Cell, newDirection: Direction) {
        this.coordinates.unshift(newHeadCell);
        this._headDirection = newDirection;
    }

    public removeTail(): Cell {
        return this.coordinates.pop();
    }

    public addPartOfBody(point: Cell) {
        this.coordinates.push(point);
    }

    public hasHorizontalDirection(): boolean {
        return Direction.RIGHT === this.headDirection || Direction.LEFT === this.headDirection;
    }

    public hasVerticalDirection(): boolean {
        return Direction.UP === this.headDirection || Direction.DOWN === this.headDirection;
    }

    public hasCollision(cell: Cell): boolean {
        return this.coordinates.includes(cell);
    }
}
