import {Cell} from "./field/cell";

export class Apple {

    private readonly _coordinates: Cell;

    constructor(coordinates: Cell) {
        this._coordinates = coordinates;
    }

    get coordinates(): Cell {
        return this._coordinates;
    }
}
