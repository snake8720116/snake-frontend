import {Snake} from "./snake";
import {Field} from "./field/field";
import {Cell} from "./field/cell";
import {Direction} from "./direction";
import {Apple} from "./apple";

export class GameObjects {

    private _field: Field;
    private _snake: Snake;
    private _apple: Apple;

    constructor() {}

    get snake(): Snake {
        return this._snake;
    }

    get field(): Field {
        return this._field;
    }

    get apple(): Apple {
        return this._apple;
    }

    // TODO эти объекты надо создавать в соответствующих фабриках
    //  а сюда передавать аргументами или в метод инициализации
    public init() {
        this._field = new Field(21, 21);
        this._snake = new Snake(this.getSnakeHeadCells(), Direction.UP);
        this.generateApple();
    }

    // TODO нужно будет вынести в фабрику
    public generateApple(): Apple {
        // TODO здесь разделить логику просечивания и создания
        this._apple = new Apple(this.field.getRandomFreedomCell());
        return this._apple;
    }

    private getSnakeHeadCells(): Cell[] {
        const centerCell = this.field.getCenterCell();
        const secondCell = this.field.getBottomRelatedCell(centerCell);
        const thirdCell = this.field.getBottomRelatedCell(secondCell);
        return [centerCell, secondCell, thirdCell];
    }
}
